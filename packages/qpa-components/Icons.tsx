import TagIcon from "@material-ui/icons/LocalOffer"
import CheckIcon from "@material-ui/icons/Check"
import CrossNoIcon from "@material-ui/icons/HighlightOff"
import PlayIcon from "@material-ui/icons/PlayCircleOutline"
import StopIcon from '@material-ui/icons/Stop';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ReplayIcon from '@material-ui/icons/Replay';


export { TagIcon, CheckIcon, CrossNoIcon, PlayIcon, StopIcon, DeleteForeverIcon, ReplayIcon }
